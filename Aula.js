import React, { useState } from 'react';
import './App.css';

function App() {
  const [results, setResults] = useState([]);

  const searchResults = (e) => {
    e.preventDefault();
    const searchTerm = e.target.elements.searchTerm.value;

    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Host': 'deezerdevs-deezer.p.rapidapi.com',
        'X-RapidAPI-Key': process.env.REACT_APP_API_KEY,
      }
    };

    fetch(`https://deezerdevs-deezer.p.rapidapi.com/search?q=${searchTerm}`, options)
      .then(response => response.json())
      .then(({ data }) => {
        console.log('data', data);
        if(data) setResults(data);
      })
  }


  return (
    <>
      <h1>Minha RádIO</h1>

      <form onSubmit={searchResults}>
        <input type="search" name="searchTerm" placeholder="Digite aqui o seu artista" />
        <button type="submit">Pesquisar</button>
      </form>

      {results.length > 0 ? 
      <ul className="list-inline">
        {results.map(result => (
          <li key={result.id}>
            <h2>{result.title}</h2>
            <img src={result.album.cover_medium} alt={result.title} />
            <br />
            <audio controls src={result.preview} />
          </li>
        ))}
      </ul> : <h2>Nenhum resultado encontrado</h2>}
    </>
  );
}

export default App;
